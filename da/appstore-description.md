ServicePlanner er en app designet til Jehovas Vidner, for at hjælpe med at holde styr på og planlægge deres forkyndelse.

* Dropbox synkronisering - tag automatisk en sikkerhedskopi af dine data og synkroniser på tværs af flere enheder.

* Integration med Territory Helper.

* I dag side - nem adgang til stopur, film, publikationer, hjemmeside browser, aktivitetsoversigt og dine planlagte besøg for dagen.

* Stopur - hold styr på den tid du bruger i tjenesten eller på andet arbejde (f.eks. LDC). Tryk på stopuret øverst på I dag skærmen eller brug 3D touch på app ikonet.

* Håndter dine genbesøg - hold styr på dine genbesøg med en kontaktliste du kan søge i. Farver er omhyggeligt udvalgt for at indikere typen af besøget og om dine genbesøg er planlagt til ude i fremtiden eller om det er længe siden de har fået besøg. Planlæg en ny dato for et genbesøg, eller tilføj et genbesøg ved at skubbe til venstre på besøget. Tryk og hold nede for at vælge og redigere flere besøg på en gang.

* Besøgshistorik og -bibliotek - få et hurtigt overblik over besøgshistorikken med en liste over de tidligere besøg. Farver bruges også her til visuelt at indikere typen af besøget. Vælg hvornår du skal komme igen, og hvad I har aftalt at tale om. Et centralt bibliotek under hvert besøg viser alle publikationer du har afsat hos dem.

* Filtrer besøg – effektiv filtrering giver dig mulighed for at gruppere og filtrere dine besøg. Find et besøg ved hjælp af mærker, sprog, område, næste besøgsdato og endda ud fra hvilke publikationer du har afsat eller ikke. 

* Kort visning - få et overblik over dine genbesøg, søg efter en adresse, placer brugerdefinerede markører for placeringen af din bil, dit distrikt, rigssalen, dit hjem og meget mere. Opret et nyt besøg ved blot at trykke og holde inde på kortet.

* Bibliotek - hold styr på dine publikationer og film. Tilføj forsidebilleder for at gøre det nemmere at genkende publikationerne. Film kan downloades til offline brug eller streames direkte for at skabe plads på enheden. Tilføj film og publikationer til I dag skærmen for nem adgang. Bemærk at Biblioteket som standard er tomt, og derfor først skal udfyldes med de publikationer du bruger i forkyndelsen.

* Hjemmesider - brug den indbyggede browser til at vise hjemmesider. Tilføj et bogmærke til dine favorithjemmesider. Understøtter Læseoversigt med temaer. Læseoversigt understøtter kinesisk Pinyin, Zhuyin, Yale og Sidney Lau over de kinesiske tegn når der læses indhold fra de officielle hjemmesider.

* Månedlige aktivitet - planlæg din daglige aktivitet med Planlagte timer og Reelle timer. Forventede timer indikerer om du når dine timer.

* Tjenesteår - få et hurtigt overblik over dine timer for hver måned og en oversigt over din aktivitet i tjenesteåret. Forventede timer indikerer om du er på rette kurs for at nå dit årlige mål.

* Målsætning - sæt dig årlige, månedlige og daglige timemål. Disse mål hjælper dig med at holde styr på dine fremskridt for måneden og året. Anvend dine daglige mål på din månedsvisning og se de Forventede timer - det er utroligt nemt at se om du når dit mål, eller om du skal ændre din planlægning.

* Forkyndermakker - planlæg hvem du skal arbejde sammen med hver dag. Kontakt din forkynder makker vha. SMS, telefon eller besked apps. Marker aftalen som bekræftet eller ej. Ikoner i kalenderen indikerer hvilke dage du har aftaler. Valgfri integration med iOS kalendere.

* Rapportering - understøtter flere timegodskrivninger og overførte minutter. Din månedlige tjenesterapport kan afleveres vha. SMS, e-mail, Telegram, WhatsApp, LINE eller Hourglass. Indsæt din gruppetilsynsmands kontaktoplysninger og hvordan han ønsker at modtage din tjenesterapport under Indstillinger.

* "Andre timer" - hold styr på de timer du bruger på LDC arbejde eller lign. Disse kan planlægges og målsættes sammen med din tjenestetid. Vælg selv et navn for timerne (f.eks. "LDC") og hvordan de skal fremstå på din tjenesterapport under Indstillinger.

---

Dette er kun begyndelsen - der er så meget mere planlagt.

Jeg vil stærkt anbefale dig at tjekke hjemmesiden for flere detaljer om funktionerne og eksempler på skærmbilleder: www.serviceplanner.me

Go' fornøjelse!

Kræver: iOS 10+
Optimeret til iPhone og iPad.
Nogle kortfunktioner kræver en internetforbindelse.
Al support og dokumentation er tilgængeligt på engelsk.