Dieses Update enthält einige Verbesserungen, die als Feedback vorgeschlagen wurden.

* Verbesserung in der Sprachenauswahlliste. Es ist eine sehr lange Liste und deswegen werden ab sofort "aktuelle" Sprachen, die du bereits verwendet hast immer ganz oben angezeigt.

* Fehler behoben, der in Zeitzonen westlich von GMT auftrat. Beim Berechnen des Tageszieles im Bereich Aktivitäten wurden die falschen Tage aktualisiert.

* Andere kleinere Verbesserungen.

Danke für das positive Feedback zu Version 1.0! Wenn du dich gedrängt fühlst eine positive Rezension im App Store zu hinerlassen, wäre das toll :)

---

Webseite: www.serviceplanner.me
Hilfe: www.serviceplanner.me/help
Issue Tracker: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp