* Integration mt Territory Helper (www.territoryhelper.com)

* Neues Plugin-Layout im Heute Bildschirm
  - Wähle welche Plugins sichtbar sind
  - Ändere die Reihenfolge der Plugins
  - Neues Aktivitäten Plugin 
  - Neues Gesamtstunden Plugin

* Ünterstützung für mehrere Predigtdienstpartner
  - Mit iOS Kalendar Integration

* Verbesserte Google Maps Integration

* Verbesserungen beim Abspielen von Videos

* Es können ab sofort zwei Telefonnummern pro Kontakt hinterlegt werden

* Es ist jetzt möglich Abgaben, Videos, Besuche von deinem Bericht zu löschen (sie aber beim Kontakt bestehen zu lassen)

* Mehrere Verbesserungen an der Bedienoberfläche und Fehlerbehebungen

Lies alles über die neuen Funktionen auf dieser Webseite:
https://serviceplanner.me/blog/

---

Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank!

Website: www.serviceplanner.me
Hilfe: serviceplanner.me/help
Issue Tracker: serviceplanner.me/issues
Facebook: facebook.com/ServicePlannerApp