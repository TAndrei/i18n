This update includes some initial tweaks based on feedback:

* Improvements to the Language lookup list. It is a long list. Now the "Recent" languages you have selected will show at the top.

* Fixed issue specific to time zones west of GMT. When applying the Daily Schedule on the Activity screen, it would update the wrong days.

* Other minor tweaks and improvements.

Thanks for your positive feedback on the version 1.0 release! If you feel inspired to write a positive review on the App Store, it would be appreciated :)

---

Website: www.serviceplanner.me
Help Docs: www.serviceplanner.me/help
Issue Tracker: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp