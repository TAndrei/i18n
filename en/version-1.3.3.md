- The Timer App Badge is now disabled by default. If you want to show the red "1" on the App icon when the Timer is running, please enable it in Settings > Timer or Settings > Notifications.

- Several bug fixes. See the website blog for details.

    - Fixed Service Year not always including custom monthly goals in Projected Hours calculation.
    
    - Fixed Carried Minutes being applied to wrong month in some Time Zones.
    
    - Fixed UI date issue when showing Time Selector in some Time Zones.
     
    - Fixed issue with imported Contacts inheriting Visits 
    
    - Minor changes to the rules for the sync of Report data