This is a quick update with just one new feature...

On the main Contact screen there is a now a “Share Contact” button. 

This is very basic at the moment. It emails the Contact details with the full Visit history. It has no option for another ServicePlanner user to import the details. That will come in the future. The focus at the moment is to provide a basic method of sharing the contact details with another publisher. You must have your device configured to send emails using the default Mail app for this button to work.

Until next time, enjoy ServicePlanner!

---

Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner, and feel inspired to leave a review, it would be greatly appreciated. Thanks for your support.

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 