Los detalles completos incluyendo capturas de pantalla y videos de esta actualización se pueden encontrar en el blog: https://serviceplanner.me/blog

Esta es una gran actualización...


Detalles de Pantallas del Informe
=================================

En la pantalla Actividad, ahora se puede hacer clic en cada línea del resumen del informe para ver más detalles.

Por ejemplo, haga clic en colocaciones para obtener un desglose completo de colocaciones por día para el mes. Puede hacer clic en cada elemento de línea para saltar al contacto o los detalles de la visita.

Esto proporciona mucha más visibilidad alrededor de su actividad para el mes y cómo se han calculado las métricas del informe.


Arrastrar y Soltar Marcadores de Contacto en el Mapa
====================================================

Ahora puede tocar en un marcador de contacto, y entrará en "modo de movimiento". Aparecerá una cruz. Ahora puede arrastrar ese marcador a una nueva ubicación.

Simplemente toque en otra parte del mapa para guardar la nueva ubicación para ese contacto.

Hubo muchas otras mejoras en el mapeo, especialmente útil para países donde las direcciones no son tan precisas o útiles. Vea el Blog para más detalles.


Filtros guardados Personalizados
================================

Ahora puede guardar sus ajustes de filtro favoritos para reutilizarlos de nuevo. En la ventana Filtro, desplácese hasta la parte inferior de la pestaña "Actual" y pulse el botón "Guardar ajustes actuales del filtro".

Ahora puede editar los filtros guardados predeterminados. Puede editar, cambiar el nombre y eliminar Filtros. También puede volver a ordenar los Filtros con el filtro Superior convirtiéndose en el filtro "Predeterminado".

En la pantalla principal de contacto toque + mantenga presionado el botón Filtro para aplicar rápidamente la configuración de Filtro predeterminado.


Vídeos
=======

Los vídeos que se muestran a un contacto ahora se pueden ver en la pestaña Biblioteca de contactos. La Biblioteca de contactos le ofrece una visión general rápida de todas las publicaciones que ha colocado con ese contacto o de todos los vídeos que se han mostrado.

Ahora puede Filtrar en Vídeos. Esto es extremadamente poderoso. Por ejemplo, utilice los filtros para mostrar todos los contactos que aún no han visto un vídeo específico.


Notas de Actividad
==================

Ahora puede introducir una Nota para cada día en la pantalla Informe Diario de Actividades. Se muestra justo debajo de los campos de Horas esto proporciona una oportunidad para explicar qué tipo de trabajo de LDC o de Consultoría que estaba haciendo, o ingrese cualquier otro detalle que le gustaría recordar para ese día.

Un resumen de estas Notas para el mes también se puede ver desde la pantalla de detalle Horas.

Las notas también se pueden incluir opcionalmente en su informe. Deberá activar esta opción en Configuración.  


Lista de Visitas
================

Al ver la lista de visitas realizadas a un contacto, la lista ahora enumerará cada publicación o vídeo de esa visita.

p.ej. Antes mostraba "1 x ¡Despertad!" pero ahora mostrará "¡Despertad! No. 3 (junio) - ¿Es la Biblia realmente de Dios?". 


Otras Mejoras
=============

Ha habido muchos otros ajustes y mejoras en la aplicación.

Por ejemplo, en Configuración> Informes, ahora puede ocultar las líneas con un valor de 0 en su informe final. Por ejemplo, si no tenías ningún curso bíblico en lugar de mostrar "Cursos Bíblicos: 0" en tu informe final, ocultará esa línea.

Para Publicaciones en la categoría "Otros", se puede elegir si la publicación se cuenta como una colocación o no. Esto podría ser útil para guardar que le ha dado una publicación al contacto, pero no debe contarla como una colocación.

También toda la aplicación ha sido traducida al español y al italiano. Muchas gracias a nuestros traductores. El trabajo en otros idiomas está en curso. Si desea ayudar a traducir ServicePlanner a su idioma, por favor, registre su interés en el Seguimiento de Problemas (ver abajo).

---

Eso es todo para esta actualización. Espero que esto le ayude a ser más eficaz en su ministerio y aprovechar al máximo el uso de ServicePlanner.

Las revisiones positivas en la App Store son increíblemente útiles. Si está disfrutando de ServicePlanner, y se siente inspirado para dejar un comentario, sería muy apreciado. Gracias por su apoyo.

---

Website: www.serviceplanner.me  
Documentos de ayuda: www.serviceplanner.me/help  
Últimas Noticias: www.serviceplanner.me/blog   
Seguimiento de Problemas: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp  