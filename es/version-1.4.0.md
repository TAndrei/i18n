Nuevo navegador Web en la aplicación:

- Se accede desde las pantallas Biblioteca y Hoy mediante la barra de herramientas superiores.
- Funciona muy similar a Safari.
- Escriba en una búsqueda una palabra o palabra para buscarla usando Google.
- Escriba una URL para acceder directamente al sitio web.
- Marque sus sitios web favoritos.
- Los marcadores se pueden ver en Cuadrícula o Vista de lista.
- Al igual que en la mayoría de los navegadores, los iconos del sitio web se detectan automáticamente.
- En la vista Cuadrícula, toque + y mantenga presionado el ícono del Marcador para editar.
- En la Vista de lista puede reordenar los Marcadores.
- Los videos compatibles se detectan y se pueden guardar en la aplicación.
- Soporte de Reader View (Vista de Lectura), similar a Safari y Firefox.
- Reader View (Vista de Lectura) contiene temas que incluyen el modo nocturno y el soporte personalizado de fuentes.
- El contenido de Reader View (Vista de Lectura) se puede enviar por correo electrónico, de forma similar a Safari.
- Reader View (Vista de Lectura) soporta Chino Pinyin, Zhuyin, Yale y Sidney. Lau por encima de los caracteres chinos al leer contenido de los sitios web oficiales de JW.

Se incluyen en esta actualización varias correcciones de errores y otras mejoras. Por favor, lea más sobre esta actualización en el blog del sitio web.

---

Espero que esta actualización sea del agrado de todo el mundo, y que usted esté disfrutando de ServicePlanner.

Las revisiones positivas en la App Store son increíblemente útiles. Si está disfrutando de ServicePlanner, y se siente inspirado para dejar un comentario, sería muy apreciado. Gracias por su apoyo.

Website: www.serviceplanner.me 
Documentos de ayuda: www.serviceplanner.me/help 
Últimas Noticias: www.serviceplanner.me/blog 
Seguimiento de Problemas: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp