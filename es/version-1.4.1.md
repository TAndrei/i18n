Esta es una actualización rápida con solo una nueva característica ...

En la pantalla principal de Contactos, ahora hay un nuevo botón "Compartir Contacto" 

Por el momento esta característica es muy básica: enviar por correo electrónico los datos de contacto con el historial completo de visitas. Esto no tiene otra opción para que otro usuario de ServicePlanner importe los detalles. Esto se implementará en el futuro. El enfoque en este momento es proporcionar un método básico para compartir los detalles de contacto con otro publicador. Para que este botón funcione, debe tener su dispositivo configurado para enviar correos electrónicos utilizando la aplicación de correo predeterminada.

Hasta la próxima, ¡disfruta de ServicePlanner!


---

Las revisiones positivas en la App Store son increíblemente útiles. Si está disfrutando de ServicePlanner, y se siente inspirado para dejar un comentario, sería muy apreciado. Gracias por su apoyo.

Website: www.serviceplanner.me 
Documentos de ayuda: www.serviceplanner.me/help 
Últimas Noticias: www.serviceplanner.me/blog 
Seguimiento de Problemas: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp