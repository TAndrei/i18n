Tightened the rules around visibility of Maps and Assignment details to better match Territory Helper.

If you can no longer see all maps for your Congregation then you need to:

  1. Change your Congregation Publisher settings in Territory Helper
  2. "Refresh Permissions" from the Territory Settings screen inside ServicePlanner

To understand the permissions better please check the Help Docs: 
https://serviceplanner.me/help/  
  
---  
  
Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner and feel inspired to leave a review, it would be greatly appreciated! Thanks for your support :)

Website: serviceplanner.me  
Help Docs: serviceplanner.me/help  
Issue Tracker: serviceplanner.me/issues  
Facebook: facebook.com/ServicePlannerApp