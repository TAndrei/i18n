Corrección de errores y mejoras.

Por favor, para obtener más detalles acerca de esta actualización, consulte el sitio web: https://serviceplanner.me/blog

---

Las revisiones positivas en la App Store son increíblemente útiles. Si está disfrutando de ServicePlanner, y se siente inspirado para dejar un comentario, sería muy apreciado. Gracias por su apoyo.

Website: www.serviceplanner.me 
Documentos de ayuda: www.serviceplanner.me/help 
Últimas Noticias: www.serviceplanner.me/blog 
Seguimiento de Problemas: www.serviceplanner.me/issues
Facebook: facebook.com/ServicePlannerApp 