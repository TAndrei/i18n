- Amélioration des performances et de la stabilité.
- Corrections de bugs mineurs.

---

Les avis positifs dans l'App Store sont très précieux et utiles. Si vous aimez ServicePlanner, n'hésitez pas à laisser un commentaire, j'apprécie par avance. Merci de votre soutien.

Website: serviceplanner.me 
Help Docs: serviceplanner.me/help 
Issue Tracker: serviceplanner.me/issues 
Facebook: facebook.com/ServicePlannerApp