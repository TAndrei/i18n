[v1.3.1] Correzioni di bug e miglioramenti.

--- 

[v1.3.0] Consulta il sito web per informazioni dettagliate su questo aggiornamento: https://serviceplanner.me/blog

*** Compagni di servizio ***  
Adesso, nel calendario dell'Attività, puoi selezionare un Compagno di servizio per ciascun giorno. Clicca sul nome per chiamarlo o per mandare un messaggio. Usa il selettore per indicare se l'appuntamento è "Confermato".

*** Promemoria rapporto ***  
Adesso puoi ricevere un promemoria per trasmettere il tuo rapporto del servizio di campo. Puoi personalizzare questo promemoria in Impostazioni > Notifiche.   

*** Promemoria visite ***  
Adesso puoi ricevere un promemoria delle prossime visite che hai programmato. Puoi personalizzare questo promemoria in Impostazioni > Notifiche.  

*** Indicatore App Badge ***  
Adesso l'icona dell'App mostra in rosso "1" per indicare che il Cronometro è in esecuzione. Puoi disabilitare questa opzione in Impostazioni > Cronometro.   

*** Importa Contatti dispositivo ***  
Se clicchi e tieni premuto sul tasto "+" nella scheda Visite, troverai l'opzione "Importa da Contatti del dispositivo". Si tratta di un'importazione basilare che permette di inserire in un Contatto i dettagli della Rubrica. Consulta il sito web per ulteriori dettagli su come funziona.

*** Messaggiare un Contatto ***
Se il contatto ha un numero di telefono, il tasto "Messaggio" permette adesso di inviare messaggi via Telegram, WhatsApp, Viber e altre applicazioni - in base alle applicazioni installate sul dispositivo.

*** Sincronizzazione in Background ***  
Adesso, quando l'App va in background, esegue un "push" di ogni modifica fatta sul dispositivo locale su Dropbox. Ciò si verifica se premi il tasto Home durante l'utilizzo di ServicePlanner o passi ad altre applicazioni o spegni il dispositivo. Questo assicura di non perdere le modifiche e che i dati siano sincronizzati tra i dispositivi.

*** Migioramento del supporto al Fuso Orario ***  
Alcuni utenti in luoghi come il Paraguay stavano riscontrando alcuni problemi. Questi sono stati risolti.

*** Portoghese ***  
ServicePlanner è adesso tradotto in Portoghese (PT)! Se vuoi aiutare a tradurre l'applicazione nella tua lingua, ti preghiamo di indicare il tuo interesse su Issue Tracker (vedi sotto).

---

Spero che questo aggiornamento sia soddisfacente per tutti e che vi stiate divertendo con ServicePlanner.

Le recensioni positive sull'App Store sono incredibilmente utili. Se ti piace l'app ServicePlanner e ti senti ispirato, sarebbe molto apprezzato se potessi lasciare una recensione. Grazie per il tuo contributo.

Sito Web: www.serviceplanner.me  
Documentazione di aiuto: www.serviceplanner.me/help  
Ultime novità: www.serviceplanner.me/blog  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp