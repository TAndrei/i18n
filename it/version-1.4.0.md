Nuovo in-app browser siti web: 

- Accessibile dalla barra degli strumenti nelle schermate Oggi e Pubblicazioni.
- Funzionamento molto simile a Safari.
- Digita un testo per eseguire una ricerca tramite Google.
- Digita un URL per accedere direttamente a un sito web. 
- Aggiungi pagine web ai tuoi Preferiti.
- Visualizza i Preferiti come Griglia o Elenco.
- Come in molti browser, le icone dei siti web sono automaticamente identificate.
- Nella visualizzazione a Griglia clicca e tieni premuto sull'icona di un Preferito per modificarlo.
- Nella visualizzazione a Elenco puoi riordinare i Preferiti.
- I video supportati sono identificati e possono essere salvati nell'App.
- Modalità Lettura supportata, simile a Safari e Firefox.
- La Modalità Lettura include temi e caratteri personalizzati.
- Il contenuto della Modalità Lettura può essere inviato per e-mail, simile a Safari. 
- La Modalità Lettura supporta i caratteri cinesi Pinyin, Zhuyin, Yale, Sidney Lau quando leggi contenuti dai siti web JW ufficiali.   

Per ulteriori informazioni su questo aggiornamento consulta il blog del sito web.

---

Spero che questo aggiornamento sia apprezzato e che vi piaccia usare ServicePlanner.

Le recenzioni positive sull'App Store sono incredibilmente utili. Se ti piace ServicePlanner, la tua recenzione sarà molto apprezzata. Grazie per il tuo contributo.

Sito Web: www.serviceplanner.me  
Documentazione di aiuto: www.serviceplanner.me/help  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 