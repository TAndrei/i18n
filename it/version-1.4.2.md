Sono felice di annunciare la traduzione in Dutch, Romanian and Japanese.

Grazie per il grande lavoro dei traduttori! Se volete aiutare a tradurre ServicePlanner nella vostra lingua scrivete per favore a support@serviceplanner.me.

Alla prossima, divertiti con ServicePlanner!

---

Le recenzioni positive sull'App Store sono incredibilmente utili. Se ti piace ServicePlanner, la tua recenzione sarà molto apprezzata. Grazie per il tuo contributo.

Sito Web: www.serviceplanner.me
Documentazione di aiuto: www.serviceplanner.me/help
Issue Tracker: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp