Affinate le regole sulla visibilità delle mappe e sui dettagli delle assegnazioni per una migliore compatibilità con Territory Helper.

Se non vuoi più visualizzare tutte le mappe della tua congregazione devi:

  1. Cambiare le "Impostazioni proclamatore" della congregazione in Territory Helper
  2. "Aggiorna autorizzazioni" dalla maschera Impostazioni Territorio di ServicePlanner

Per capire meglio le autorizzazioni, si prega di consultare la guida: 
https://serviceplanner.me/help/  
  
---  
  
Le recenzioni positive sull'App Store sono incredibilmente utili. Se ti piace ServicePlanner, la tua recenzione sarà molto apprezzata. Grazie per il tuo contributo.

Sito Web: www.serviceplanner.me
Guida: serviceplanner.me/help
Issue Tracker: serviceplanner.me/issues
Facebook: facebook.com/ServicePlannerApp