* Integrazione con Territory Helper (www.territoryhelper.com)

* Nuova impaginazione Plugin nella schermata Oggi
  - Scegli quali Plugins abilitare
  - Decidi l'ordinamento dei Plugins
  - Nuovo Plugin Attività
  - Nuovo Plugin Sommario Ore

* Supporto per Compagni di servizio multipli
  - Con integrazione nel Calendario iOS

* Migliorata l'integrazione con Google Maps

* Miglioramenti nella riproduzione dei Video

* Possibilità di aggiungere 2 numeri di telefono per Contatto

* Possibilità di eliminare Pubblicazioni, Video e Visite dal tuo rapporto (ma lasciarli nel Contatto)

* Diversi miglioramenti e problemi dell'interfaccia utente risolti

Ti invitiamo a leggere in merito a queste nuove caratteristiche sul sito web:
https://serviceplanner.me/blog/

---

Le recenzioni positive sull'App Store sono incredibilmente utili. Se ti piace ServicePlanner, la tua recenzione sarà molto apprezzata. Grazie per il tuo contributo.

Sito Web: www.serviceplanner.me
Documentazione di aiuto: serviceplanner.me/help
Issue Tracker: serviceplanner.me/issues
Facebook: facebook.com/ServicePlannerApp