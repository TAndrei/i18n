誠意ある翻訳者の方々の協力により、日本語、ドイツ語、ルーマニア語で、アプリの利用が可能になりました。

是非、Service Plannerのアプリを活用して、効果的な奉仕を楽しんでください。

他の言語への翻訳を手伝ってくださる方は、support@serviceplanner.me までご連絡ください。

アプリを気に入って頂けましたら、いいね評価とレビューをして頂けましたら大変助かります！

ウエブサイト: serviceplanner.me 
ヘルプサイト: serviceplanner.me/help 
問題報告: serviceplanner.me/issues 
Facebook: facebook.com/ServicePlannerApp 
*サイトはすべて英語となります