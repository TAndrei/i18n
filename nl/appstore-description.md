ServicePlanner is een Velddienst en Nabezoek Management app van de ontwikkelaar van Equipd. 

Ontwikkeld voor Jehovah's Getuigen, ServicePlanner combineert krachtige features met een aantrekkelijke interface. Het doel is niet alleen maar tijd bijhouden, maar om je tijd er toe te laten doen. Plan en noteer je tijd vanaf vandaag!

* Dropbox Synchronisatie - Automatische backup van je data, en synchronisatie tussen meerdere apparaten.

* Vandaag Scherm - Snelle toegang to Videos, Publicaties en je geplande nabezoeken voor vandaag. 

* Timer - Je tijd in de velddienst bijhouden of  "ander werk (PBA etc) dmv de timer. Start/Pause/Resume/Stop de timer in de app door middel van force touch 3d.

* Nabezoek Management - Beheer al je Nabezoeken dmv een doorzoekbare lijst. Verschillende kleuren om je Contact Type aan te geven en om te kijken of je op tijd bent met het bezoeken. Veeg over een nabezoek om een afspraak opnieuw te plannen, of om een nabezoek toe te voegen. Houd een Nabezoek ingedrukt om multi-select mode te starten en meedere nabezoeken tegelijk te bewerken.

* Nabezoek Geschiedenis & Bibliotheek - Snel toegang tot al je nabezoeken, inclusief een lijst van vorige bezoeken. Kleuren worden weer gebruikt om je visueel te informeren over je nabezoek type. Definieer je volgende bezoek, datums en planning. Een centrale bibliotheek voor elk nabezoek, laat zien wat je allemaal bij hun hebt achtergelaten.

* Nabezoek Filteren - Krachtige filters stellen je in staat om je nabezoeken te ordenen. Doorzoek je nabezoeken dmv tags, Taal, Plaats, Volgende Bezoek, en Publicaties verspreid.

* Kaart Weergave - Krijg een overzicht van al je nabezoeken, zoek naar adressen, plaats markers voor de locatie van je auto, Gebied, Koninkrijkszaal, je woonplaats en nog meer. Als je een locatie ingedrukt houdt kun je rechtstreekt een nieuw nabezoek maken.

* Bibliotheek - Beheer al je publicaties en video's die je bij een nabezoek achterlaat. Omslag design om je publicaties en filmpjes te indentificeren. Video locaties kunnen worden ingevoerd om te kunnen downloaden voor offline gebruik, of om te streamen en opslag ruimte te besparen. Voeg items toe aan je Vandaag scherm.

* Maandelijkse Activiteit - Plan je dagelijkse activiteit, Geplande uren en Werkelijke uren. Bekijk een overzicht van je uren tot nu toe. Geplande uren laten zien of je op schema loopt deze maand, en of dat zo blijft met je huidige uren rapportage.

* Dienstjaar - Zie in een keer je uren per maand, en een overzicht van je activiteit voor het huidige dienstjaar. Een overzicht laat zien of je op schema loopt voor het huidige dienstjaar.

* Plannen - Plan je Jaarlijkse, Maandelijkse, en dagelijkse uren. Deze informatie zal worden gebruikt om je vooruitgang voor de huidige maand en het jaar te kunnen zien. Pas je dagelijkse uren schema toe, en zie welke uren je zult bereiken die maand - duidelijk overzicht of je je uren zult halen die maand, of dat je misschien wat wijzigingen zal moeten aanbrengen.

* Raporteren - Volledige ondersteuning voor meerdere uren rapportage, of minuten overdragen naar de volgende maand. Je maandelijkse rapport kan worden verzonden dmv: E-mail, sms, Telegram of WhatsApp. Stel je groepsopziener contact gegevens in op het instellingen scherm. 

* "Overige Uren" - Hou je overige uren bij zoals: PBA Werk of werk voor Bethel. Deze kunnen worden gepland naast je maandelijkse uren. Configureer een catagorie naam: (bv "PBA") en rapport opties in instellingen.

---

Dat is nog maar een kort overzicht van de huidige features van ServicePlanner version 1.0. dit is nog maar het begin - er zijn nog zoveel features gepland.

Ik moedig je aan om de website te bezoeken voor verdere detals en features en voorbeeld screenshots: www.serviceplanner.me

Veel Plezier!

Vereisten: iOS 10+
Geoptimaliseerd voor iPhone en iPad.
De UI is momenteel alleen in het Engels en Duits.
Hulp word gegeven in Engels, maar ik werk ook aan andere talen.
Kaarten hebben offline caching. Maar over het algemeen heb je een met internet verbonden apparaat nodig.