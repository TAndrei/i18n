Ik ben blij om te mogen aankondigen dat ServicePlanner nu beschikbaar is Nederlandse, Roemeense en Japanse gebruikers.

Bedankt aan de hardwerkende vertalers! Als u wilt helpen ServicePlanner in uw taal te vertalen, stuur dan een e-mail naar support@serviceplanner.me.

Tot de volgende keer, geniet van ServicePlanner!

---

Positieve beoordelingen in de App Store zijn een ongelooflijke hulp. Als u van ServicePlanner geniet en u geïnspireerd voelt om een beoordeling achter te laten, dan zou ik zeer op prijs stellen. Bedankt voor uw steun.

Website: www.serviceplanner.me  
Help Documenten: www.serviceplanner.me/help  
Probleem Oplosser (Engels): www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 