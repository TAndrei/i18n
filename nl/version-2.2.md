Nieuwe opties voor Delen van Contactpersonen
============================

* Delen met behulp van de samenvatting e-mail bevat nu een bestandsbijlage die een andere ServicePlanner-gebruiker kan gebruiken om de contactpersoon rechtstreeks te importeren

* U kunt alleen het bestand delen (zonder de e-mail) en verzenden via AirDrop of met berichten-apps zoals Telegram, WhatsApp, Viber enz.


Nieuwe Handmatige backup / herstellen
=======================================

Als een alternatief voor Dropbox Sync, kunt u nu de nieuwe handmatige back-up optie gebruiken. Verzend de back-up via AirDrop naar een ander apparaat of sla het op met een cloudservice of deel het bestand via een andere app. U kunt de back-up ook e-mailen via de standaard Mail-app die de gratis Mail Drop-oplossing zal gebruiken om het bestand 30 dagen op te slaan. Het punt is - je mag kiezen.

Om de back-up te herstellen, gaat u eenvoudig naar het ZIP-bestand en kiest u voor "Openen in ..." ServicePlanner.

De functie Handmatige back-up omvat een optionele herinneringsservice - u kunt kiezen om elke 7, 14, 30 of 60 dagen eraan te worden herinnerd.

U kunt meer lezen over deze nieuwe functies op het ServicePlanner-blog waarin deze meer in detail worden uitgelegd.


---

Positieve beoordelingen in de App Store zijn ongelooflijk handig. Als u van ServicePlanner geniet en u bent geïnspireerd om een reactie achter te laten, zou dit zeer op prijs worden gesteld! Bedankt voor je steun :)

Website: serviceplanner.me  
Help Docs: serviceplanner.me/help  
Issue Tracker: serviceplanner.me/issues  
Facebook: facebook.com/ServicePlannerApp