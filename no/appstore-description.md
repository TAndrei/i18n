APP STORE DESCRIPTION  
This is used on the main app page on the App Store.  
It must be less than 4,000 characters in length.  

Only translate below the following line...  

---

ServicePlanner er en felttjeneste og gjenbesøk hånterings app designet for Jehovas vitner.

* Dropbox Sync - automatisk backup med syncronisering på tvers av flere enheter.

* Integrasjon med Territory Helper.

* Idag - rask tilgang til Timer, Videoer, Publikasjoner, Nettstedet, Aktivitets Planleggeren og dine planlagte gjenbesøk for dagen. 

* Timer - noter tiden din brukt i tjenesten eller på "annet" arbeid (f.eks. LDC). Trykk på tid-teller-linjen i Idag fanen eller 3D touch app ikonet for å starte timeren.

* Kontaktliste - håndter dine gjenbesøk med en søkbar Kontaktliste. Farger er nøye valgt ut for å vise Kontakt type, også for lett å kunne se om det er lenge siden sist eller om det går etter planen. Skyv kontakten til side for lett å endre tidspunkt for gjenbesøket eller planlegge et nytt. Trykk og hold for å åpne muligheten for å velge flere kontakter, disse kan redigeres samtidig.

* Kontakt besøkshistorikk - se raskt hvem du har vært hos med en liste over siste besøk. Farger er også her brukt for å vise besøkstype. Gjør klar for neste besøksdato og samtaletema. Du kan lett se en historikk over alle publikasjoner levert til de enkelte gjenbesøkene.

* Kontakt Filtrering - sorter gjenbesøkene etter Gruppe eller Type. Søk ved hjelp av Tagger, Språk, Nærmest, Neste Besøksdato, og Publikasjoner Levert eller Ikke Levert.

* Kart - få et overblikk over Kontaker, søk etter adresser, plasser bilen din, Distriktet, Riketssal, Hjem med mer. Du kan trykke og holde på kartet for å lage ny kontakt på ønsket adresse. 

* Bibliotek - håndter Publikasjoner og Videoer. Om ønsket kan du legge til bilde av Publikasjonen eller Videoen for å letter kjenne dem igjen. Videoer kan lagres lokalt eller streames. Legg til det du ønsker av litteratur på Idag fanen. NB! Biblioteket er tomt fra start, du må selv legge inn litteraturen du trenger.

* Nettsted - Bruk den innebygde nettleseren for å vise nettsider. Lagre favorittsidene dine som Bokmerker. Støtter Lese Visning med Temaer. Lese Visning støtter Kinesisk Pinyin, Zhuyin, Yale, Sidney Lau over Kinesiske karakterer mens du leser innhold fra de offesielle nettsidene.

* Månedlig Aktivitet - planlegg dagene dine med Planlagte Timer vs Fullførte Timer. Prosjekterte Timer viser om du er på rett spor med timene dine.

* Tjenesteåret - få ved et øyekast oversikten over timer i måneden og en oppsumering av aktiviteten din for Tjenesteåret. Prosjekterte Timer viser om du er på rett vei til å treffer målene dine for året. 

* Planlegging - definer dine Årlige, Månedlige og Daglige Time Mål. Disse vil så bli brukt til å følge med på dine framskritt for Måneden og Året. Legg Dagens Plan til Måneden for å se Prosjekterte Timer - dette gjør det lett å se om du får nok timer eller må endre på planene.

* Feltpartner - sorter avtalene dine for hver dag. Kontakt Feltpartneren på SMS, Telefon, eller tekstmeldingsapper. Marker avtalen som bekreftet eller ikke. Ikonene i kalenderen gjør forskjell på dager med en avtale og dager med flere. Disse kan også legges til i iOS kalenderen.

* Rapportering - støtte for flere Timer til gode og Roll-Over Minutt. Månedsrapporten kan sendes som Epost, TXT, Telegram, WhatsApp, LINE og Hourglass. Legg inn din tjenestegruppetilsynsmann og endre dine rapporteringsvalg i Innstillinger. 

* "Andre Timer" - loggfør dine andre ikke-tjeneste timer som f.eks. LDC. Disse kan settes opp og planlegges ved siden av dine Tjeneste Timer. Konfigurer kategori navnet (eks "LDC") og rapporteringsvalg i Innstillinger.

---

This is just the beginning - there are so many more features planned.

I strongly encourage you to check out the website which further details the features and shows example screenshots: www.serviceplanner.me

Enjoy!

Requirements: iOS 10+
Optimised for the iPhone and iPad.
Some mapping features require a connected device.
All support and documentation is provided in English.