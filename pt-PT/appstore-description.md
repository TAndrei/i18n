ServicePlanner é uma aplicação para a Gestão do Serviço de Ministério e das Visitas efetuadas na pregação.

Desenhado para as Testemunhas de Jeová, ServicePlanner combina vastos recursos com um interface intuitivo e atrativo. O objetivo não é apenas contar o tempo, mas fazer o tempo contar. Planeie e programe o seu ministério a partir de hoje!

* Sincronização Dropbox - Faça Backup e sincronize os seus dados automaticamente em todos os seus equipamentos.

* Ecrã Hoje - rápido acesso aos Videos, Publicações e às visitas agendadas para hoje.

* Temporizador - Registe o tempo gasto no ministério ou em "outro" serviço (LDC,etc) com o temporizador. Inicie/Pause/Retome/Pare o temporizador no icon da aplicação ao usar o "Force Touch".

* Gestão de Contactos - Centralize a gestão das suas visitas com a função de procura na lista de contactos. Cores foram cuidadosamente usadas para indicar o tipo de contacto e se a sua visita está conforme agendada ou se está atrasada. Deslize sobre um contacto e reagende rápidamente a sua próxima visita ou adicione uma Revisita. Pressionar + Manter para realizar multipla escolha e editar multiplos contactos de uma só vez.

* Biblioteca e Histórico de Visitas - Aceda rapidamente ao seu histórico de visitas com uma lista de visitas anteriores. A cor novamente é usada para indicar visualmente o tipo de visita. Defina a sua próxima data de visita e planos. Uma biblioteca Central para cada contato mostra todas as publicações que já colocou com elas.

* Filtros de contatos - poderoso filtro permite que defina como agrupa e classifica os seus contatos. Procure Contatos usando Etiquetas, Idioma, Localidade, Data da Próxima Visita, até mesmo por Publicações Colocadas ou Não Colocadas.

* Vista do mapa - obtenha uma visão geral de suas visitas, procure por endereços, coloque marcadores personalizados para a localização do seu carro, território, salão do reino, sua casa e muito mais. Pode pressionar + Manter para criar um novo contato diretamente de um local do mapa.

* Biblioteca - Gerencie Centralmente as Publicações e Vídeos que colocará nas suas visitas. Adicione as capas para identificar visualmente as publicações e vídeos. Os locais de vídeo podem ser inseridos para permitir que faça o download para uso off-line, ou para visualizar diretamente e libertar espaço de armazenamento no seu dispositivo. Adicione itens ao Ecrã Hoje.

* Atividade mensal - Planeie a sua atividade diária com horas planeadas vs horas reais. Visualize imediatamente o progresso do seu relatório. Horas projetadas indicam se está em linha com o o seu tempo até à data ou se continua conforme o seu plano.

* Ano de Serviço - veja de relance as suas horas por mês e um resumo de sua atividade para o Ano do Serviço. Horas projetadas indicam se estão de acordo a cumprir o seu objetivo de horas para o ano.

* Agendamento - defina seus objetivos anuais, mensais e diários. Estes serão usados para acompanhar o seu progresso para o mês e para o ano. Aplique seu horário diário ao seu mês e veja as Horas projetadas - é tão fácil de ver se conseguirá atingir os tempos ou se precisa de alterar a sua agenda.

* Relatórios - Suporte para crédito de horas. O seu Relatório Mensal pode ser enviado por Email, SMS, Telegram ou WhatsApp. Configure os detalhes do seu surperientendente e as opções do relatório nas Configurações.

* "Outras Horas" - registre suas horas não ministeriais, como o trabalho de Consulta da LDC ou da Filial. Estes podem ser programados e planeados ao longo das Horas do Ministério. Configure o nome da categoria (por exemplo, "LDC") e as opções de Relatório em Configurações.

---

Este é apenas o inicio - temos muitos mais recursos planeados para implementar.

Desfrute!

Requisitos: iOS 10+
Otimizado para iPhone e iPad.
O Suporte é dado em Inglês, mas vou tentar em outros idiomas.
Os mapas possuem algumas funções em modo offline, mas em geral necessita de uma ligação à internet.