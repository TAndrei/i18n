Ajustadas as regras de visibilidade dos territórios e detalhes da designação dos mesmos de forma a corresponder melhor com o Territory Helper.

Se agora não conseguir visualizar todos os territórios da sua congregação deverá:

  1. Mudar as definições de Publicador da sua congregação no Territory Helper
  2. "Atualizar Permissões" na aba das definições dos territórios no ServicePlanner

Incentivo-o a ler sobre estes novos recursos no site:
https://serviceplanner.me/blog/

---

Avaliações positivas na App Store são incrivelmente úteis. Se está a gostar do ServicePlanner, e se sentir inspirado para deixar um comentário, seria muito apreciado. Obrigado pelo seu apoio.

Website: www.serviceplanner.me  
Documentos de Ajuda: www.serviceplanner.me/help  
Registo de Problemas: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp