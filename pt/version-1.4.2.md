Tenho o prazer de anunciar a disponibilidade para usuários holandeses, romenos e japoneses.

Graças ao árduo trabalho dos tradutores! Se gostasse de ajudar a traduzir o ServicePlanner para o seu idioma, por favor envie um email para support@serviceplanner.me.

Até a próxima, desfrute do ServicePlanner!

---

Avaliações positivas na App Store são incrivelmente úteis. Se está a gostar do ServicePlanner, e se sentir inspirado para deixar um comentário, seria muito apreciado. Obrigado pelo seu apoio.

Website: www.serviceplanner.me
Documentos de Ajuda: www.serviceplanner.me/help
Registo de Problemas: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp