ServicePlanner este o aplicaţie pentru Serviciul de teren şi Administrare contacte proiectată pentru Martorii lui Iehova.

* Sincronizare Dropbox - salvarea automată a datelor şi sincronizarea pe mai multe dispozitive.

* Ecranul Astăzi - accesul rapid la mt. video, publicaţii şi vizite planificate în ziua respectivă.

* Cronometru - înregistrarea timpului petrecut în predicare sau în "alte" repartiţii (LDC etc). Atingeţi bara cronometrului din ecranul Astăzi sau atingeţi pictograma aplicaţiei.

* Administrare Contacte - administrarea centralizată a vizitelor cu o listă de căutare a Contactelor. Culorile sunt folosite cu grijă pentru a indica Tipul vizitei, dacă este întârziată sau la zi. Glisează un contact pentru a reprograma rapid următoarea vizită, sau pentru a adăuga o vizită ulterioară. Menţine apăsat pentru a intra în modul de selectare multiplă şi editează mai multe persoane simultan.

* Istoric vizite & Bibliotecă - accesarea rapidă a istoricului vizitei cu o listă a vizitelor precedente. Culoarea este din nou folosită pentru a indica Tipul vizitei. Stabilirea planurilor şi Data următoarei vizite. O bibliotecă centralizată a fiecărei persoane vizitate arată publicaţiile plasate acesteia.

* Filtrare contacte - filtrarea puternică permite definirea modului de grupare şi sortare a vizitelor. Caută persoanele vizitate utilizând Etichete, Limba, Localitatea, Data următoarei vizite, inclusiv Publicaţiile plasate sau Neplasate.

* Vizualizare hartă - prezentare generală a vizitelor, căutare adrese, plasare marcaje personalizate pentru Locaţia automobilului, a Teritoriului, a Sălii Regatului, a Locuinţei şi multe altele. Menţineţi apăsat pe o locaţie pentru a crea un contact nou direct de pe hartă.

* Bibliotecă - administrarea centralizată a publicaţiilor şi materialelor video care le vei plasa la vizite. Adăugare Copertă pentru a identifica vizual publicaţiile şi materialele video. Pot fi folosite locaţii video pentru a permite descărcarea şi utilizarea offline, sau pentru redare online şi eliberarea spaţiului de stocare a dispozitivului. Adăugare elemente la ecranul Astăzi. Reţineţi că Biblioteca în mod implicit este goală, utilizatorii o vor umple singuri.

* Site-uri web - utilizarea browserului încorporat pentru a afişa conţinutul web. Marcarea paginilor preferate. Suport Vizualizare cititor. Vizualizare cititor suportă caractere Pinyin, Zhuyin, Yale, Sidney Lau deasupra caracterelor chineze când citeşte conţinutul de pe site-urile oficiale.

* Activitate lunară - planificarea activităţii zilnic cu Ore planificate versus Ore actuale. Orele preconizate indică dacă eşti la zi cu timpul.

* An de serviciu - afişarea rapidă a orelor lunare, şi rezumatul activităţii pentru Anul de serviciu. Orele preconizate indică dacă sunteţi pe punctul de a atinge Obiectivul orelor pentru anul respectiv.

* Planificare - stabilirea obiectivului orelor Anual, Lunar şi Zilnic. Acestea vor fi apoi folosite pentru urmărirea progresului Lunar şi Anual. Aplică Planificarea zilnică pentru luna în curs şi vizualizează Orele preconizate - e atât de uşor de observat dacă vă vei reuşi cu orele sau dacă e nevoie să modifici planificarea.

* Parteneri de lucrare - stabilirea partenerilor pentru fiecare zi. Contactaţi partenerii prin mesaj SMS, Telefon sau aplicaţii de mesagerie.  Marcaţi aranjamentul ca confirmat sau nu. Pictogramele din calendar indică zilele cu partenerii stabiliţi.

* Raportare - suport complet pentru mai multe credite de ore şi reportarea minutelor rămase. Raportul lunar poate fi trimis folosind Email-ul, SMS-ul, Telegrama sau WhatsApp. Configuraţi detaliile de contact ale supraveghetorului şi Opţiunile raportului din Setări.

* "Alte ore" - înregistrarea creditului de ore, cum ar fi o repartiţie LDC sau Consultant pentru Betel. Acestea pot fi programate şi planificate pe lângă Orele din predicare. Configuraţi numele categoriei (de ex. "LDC") şi Opţiunile raportului din Setări.

---

Acesta este doar începutul - sunt planificate multe alte caracteristici.

Vă recomand să consultaţi site-ul care detaliază aceste caracteristici şi prezintă unele exemple şi capturi de ecran: www.serviceplanner.me

Bucuraţi-vă!

Cerinţe: iOS 10+ Optimizat pentru iPhone şi iPad. Unele caracteristici ale hărţii necesită un dispozitiv conectat. Tot suportul şi documentaţia este oferită în engleză. Interfaţa utilizatorului este tradusă în germană, italiană, spaniolă, portugheză, olandeză, română şi japoneză.
