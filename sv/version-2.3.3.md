• Ny meny under Aktivitet > År som tillåter manuell uppdatering av den Årliga datan för rapporter.

• Appen stöder nu Norska - Tack John!

• Buggfixar och förbättringar.

Besök hemsidan för alla detaljer angående denna uppdatering: www.serviceplanner.me/blog

Positiva recensioner i App Store är väldigt uppskattade. Om du gillar ServicePlanner, och känner dig manad att lämna en recension, så uppskattas det mycket. Tack för ditt stöd.

---

Hemsida: www.serviceplanner.me  
Dokumentation: www.serviceplanner.me/help  
Senaste nytt: www.serviceplanner.me/blog
Fel/Buggar: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_