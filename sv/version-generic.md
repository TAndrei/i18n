Buggfixar och förbättringar.

Besök hemsidan för alla detaljer angående denna uppdatering.
www.serviceplanner.me/blog

Positiva recensioner i App Store är väldigt användbara. Om du gillar ServicePlanner, och känner dig manad att lämna en recension, så uppskattas det mycket. Tack för ditt stöd.

---

Hemsida: www.serviceplanner.me  
Dokumentation: www.serviceplanner.me/help  
Senaste nytt: www.serviceplanner.me/blog
Fel/Buggar: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp  